import {Component} from '@angular/core';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  qrData = null;
  createdCode = null;
  scannedCode = null;

  constructor(private barcodeScanner: BarcodeScanner) {

  }

  createCode() {
    this.createdCode = this.qrData;
  }

  scanCode() {
    this.barcodeScanner.scan().then((data) => {
      this.scannedCode = data.text;
    }, (err) => {
      console.log('Error: ', err);
    });
  }
}
